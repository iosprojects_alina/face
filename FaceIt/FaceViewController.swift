//
//  ViewController.swift
//  FaceIt
//
//  Created by Alina Chernenko on 10/21/16.
//  Copyright © 2016 dimalina. All rights reserved.
//

import UIKit

class FaceViewController: UIViewController {

    var expression = FacialExpression(eyes: .Close, eyeBrows: .Relaxed, mouth: .Smirk) {
        didSet{
            updateUI()
        }
    }
    
    @IBOutlet weak var faceView: FaceView! {
        didSet{
            faceView.addGestureRecognizer(UIPinchGestureRecognizer(target: faceView, action: #selector(FaceView.changeScale(gesture:))))
            let happierSwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(FaceViewController.increaseHappiness))
            happierSwipeGestureRecognizer.direction = .up
            faceView.addGestureRecognizer(happierSwipeGestureRecognizer)
            
            let sadderSwipeGestureRecognizer = UISwipeGestureRecognizer(target: self, action: #selector(FaceViewController.sadderHappiness))
            sadderSwipeGestureRecognizer.direction = .down
            faceView.addGestureRecognizer(sadderSwipeGestureRecognizer)

            updateUI()
        }
    }
        
    private let mouthCurvatures = [FacialExpression.Mouth.Frown: -1, .Grin: 0.5, .Neutral:0.0 , .Smile: 1.0, .Smirk: -0.5]
    private let eyeBrowTilts = [FacialExpression.EyeBrows.Furrowed: -0.5, .Relaxed: 0.5, .Normal:0.0]
    func updateUI(){
        if faceView != nil{
            switch expression.eyes {
            case .Open:
                faceView.eyesOpen = true
            case .Close:
                faceView.eyesOpen = false
            case .Squinting:
                faceView.eyesOpen = false
            }
        }
        faceView?.mouthCurvatire = mouthCurvatures[expression.mouth] ?? 0.0
        faceView?.eyeBrowTilt = eyeBrowTilts[expression.eyeBrows] ?? 0.0
    }
    
    @IBAction func toggleEyes(_ sender: UITapGestureRecognizer) {
        
        if sender.state == .ended {
            switch expression.eyes {
            case .Open:
                expression.eyes = .Close
            case .Close:
                expression.eyes = .Open
            default:
                break
            }
        }
    }
    
    private struct Animation {
        static let ShakeAngle = CGFloat(M_PI/6)
        static let ShakeDuration = 0.5
    }
    
    @IBAction func headShake(_ sender: UITapGestureRecognizer) {
      UIView.animate(
        withDuration: Animation.ShakeDuration,
        animations: {
            self.faceView.transform = CGAffineTransform(rotationAngle: Animation.ShakeAngle)
      },
        completion: { (finished) in
            if finished {
                UIView.animate(
                    withDuration: Animation.ShakeDuration,
                    animations: {
                        self.faceView.transform = CGAffineTransform(rotationAngle: -Animation.ShakeAngle)
                },
                    completion: { (finished) in
                        if finished {
                            UIView.animate(
                                withDuration: Animation.ShakeDuration,
                                animations: {
                                    self.faceView.transform = CGAffineTransform(rotationAngle: 0)
                            },
                                completion: { (finished) in
                                    
                            }
                            )
                        }
                }
                )
            }
        }
     )
    }
    
    func increaseHappiness(){
        expression.mouth = expression.mouth.happierMouth()
    }
    
    func sadderHappiness(){
        expression.mouth = expression.mouth.sadderMouth()
    }
    
    }
