//
//  BlinkingViewController.swift
//  FaceIt
//
//  Created by Alina Chernenko on 2/21/17.
//  Copyright © 2017 dimalina. All rights reserved.
//

import UIKit

class BlinkingViewController: FaceViewController {
    
    var blinking: Bool = false {
        didSet{
            startBlink()
        }
    }
    
    private struct BlinkRate {
        static let ClosedDuration = 0.4
        static let OpenDuration = 2.5
    }
    
     func startBlink() {
        if blinking {
            faceView.eyesOpen = false
            Timer.scheduledTimer(
                timeInterval: BlinkRate.ClosedDuration,
                target: self,
                selector: #selector(endBlink),
                userInfo: nil,
                repeats: false)
        }
    }
    
    func endBlink(){
        faceView.eyesOpen = true
        Timer.scheduledTimer(
            timeInterval: BlinkRate.OpenDuration,
            target: self,
            selector: #selector(startBlink),
            userInfo: nil,
            repeats: false)
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        blinking = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        blinking = false
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

}
